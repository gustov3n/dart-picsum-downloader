import 'dart:io';

import 'package:dart_picsum_downloader/dart_picsum_downloader.dart' as downloader;

void main(List<String> arguments) async {
  stdout.writeln("Hello there, i can fetch you some random images ;)");
  stdout.writeln("Just say how many images do you need?");

  int count = int.tryParse(stdin.readLineSync() ?? "") ?? 0;

  if (count <= 0) {
    stdout.writeln("Sorry i only can fetch at least 1 image");
    return;
  }

  stdout.writeln("Would you like to specify the resolution? (y/n)");
  bool specifyResolution = stdin.readLineSync()?.toLowerCase() == "y";

  int width = 200;
  int height = 300;
  if (specifyResolution) {
    stdout.writeln("How much is the width (in pixel)?");
    width = int.tryParse(stdin.readLineSync() ?? "") ?? 0;
    if (width <= 0) {
      stdout.writeln("Sorry but minimum width is 1 pixel");
      return;
    }

    stdout.writeln("How much is the height (in pixel)?");
    height = int.tryParse(stdin.readLineSync() ?? "") ?? 0;
    if (height <= 0) {
      stdout.writeln("Sorry but minimum height is 1 pixel");
      return;
    }
  }

  stdout.writeln("Okay let me work it out for you :D");
  
  String imagePath = "${Directory.current.path}/images";
  Directory imageDir = Directory(imagePath);
  var files = imageDir.listSync();
  for (var file in files) {
    file.deleteSync();
  }

  for (var i = 0; i < count; i++) {
    var fetchImages = await downloader.fetchImages(
      i,
      width: width,
      height: height,
    );

    if (fetchImages) {
      stdout.writeln("I've got ${i + 1} image${i > 0 ? "s" : ""}");
    } else {
      break;
    }
  }

  stdout.writeln("There, you can check your images in '/images' directory");
  stdout.writeln("Thank you for using me <3");
}

import 'dart:io';

import 'package:http/http.dart' as http;

Future<bool> fetchImages(
  int index, {
  int width = 200,
  int height = 300,
}) async {
  var url = Uri.parse("https://picsum.photos/$width/$height");

  try {
    var response = await http.get(url);

    if (response.statusCode == 200) {
      File file = File('images/image_$index.jpg');
      await file.writeAsBytes(response.bodyBytes);
      return true;
    }
  } catch (e) {
    stdout.writeln("I'm sorry an error has occured ;(");
  }

  return false;
}
